console.log("==================================== IF ELSE =======================================");
var nama = 'Esa Dwi';
var peran = 'werewolf';

if( nama == '' ){
  console.log('Nama harus diisi!');
}else if( peran == '' ){
  console.log('Halo '+nama+', Pilih peranmu untuk memulai game!');
}else if( peran.toLowerCase() == 'penyihir' ){
  console.log("Selamat datang di Dunia Werewolf, "+nama);
  console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
}else if( peran.toLowerCase() == 'guard' ){
  console.log("Selamat datang di Dunia Werewolf, "+nama);
  console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
}else if( peran.toLowerCase() == 'werewolf' ){
  console.log("Selamat datang di Dunia Werewolf, "+nama);
  console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!.");
}else{
  console.log("Peran "+peran+" tidak ditemukan");
}


console.log("==================================== SWITCH CASE =======================================");
var hari = 2; 
var bulan = 12; 
var tahun = 2200;
var namaBulan = '';

switch(bulan){
	case 1: namaBulan = 'Januari'; break;
	case 2: namaBulan = 'Februari';break;
	case 3: namaBulan = 'Maret'; break;
	case 4: namaBulan = 'April'; break;
	case 5: namaBulan = 'Mei'; break;
	case 6: namaBulan = 'Juni'; break;
	case 7: namaBulan = 'Juli'; break;
	case 8: namaBulan = 'Agustus'; break;
	case 9: namaBulan = 'September'; break;
	case 10: namaBulan = 'Oktober'; break;
	case 11: namaBulan = 'November'; break;
	case 12: namaBulan = 'Desember'; break;
}

if( hari == 0 || hari > 31 ){
  console.log("Range hari 1-31");
}else if( bulan == 0 || bulan > 12 ){
  console.log("Range bulan 1-12");
}else if( tahun < 1900 || tahun > 2200){
  console.log("Range tahun 1900-2200");
}else{
  console.log(hari+' '+namaBulan+' '+tahun);
}