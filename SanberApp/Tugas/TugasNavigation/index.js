import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import {LoginScreen} from '../Tugas14/LoginScreen';
import {AboutScreen} from '../Tugas14/AboutScreen';
import SkillScreen from '../Tugas14/SkillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';

const Tabs = createBottomTabNavigator();

// Screen Home
export const LoginPage = ({ navigation }) => (
    <LoginScreen navigation={navigation}></LoginScreen>
)

// Screen Detail
export const AboutPage = ({ navigation }) => (
    <AboutScreen navigation={navigation}></AboutScreen>
)


// Screen Profile
export const SkillPage = ({ navigation }) => (
    <SkillScreen></SkillScreen>
)

export const ProjectPage = ({ navigation }) => (
    <ProjectScreen></ProjectScreen>
)

export const AddPage = ({ navigation }) => (
    <AddScreen></AddScreen>
)


