
import React, { Component } from 'react';
import {Platform, StyleSheet,Text,View,Image,TouchableOpacity,FlatList} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/videoItem';
import data from './data.json';

export default class App extends Component {

	renderItem = (video) => {
		return (<VideoItem video={video.item} />)
	}

	itemSeparator = () => {
		return (<View style={{height:1,backgroundColor:'#CCCCCC'}}/>);
	}

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image source={require('./images/logo.png')} style={{ width: 98, height: 22 }} />
        </View>
        <View style={styles.body}>
          <FlatList
		  data={data.items}
		  renderItem={this.renderItem}
          keyExtractor={(item)=>item.id}
          ItemSeparatorComponent={this.itemSeparator}
           />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4
  }
});