import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, TextInput, Icon, TouchableOpacity } from 'react-native'

export default class LoginScreen extends Component {

	render(){

		return(
			
			<View style={styles.container}>
				<View>
					<Image source={require('./images/logo.png')} style={styles.logo} />
				</View>
				<TextInput style={styles.inputBox}
					placeholder="Email"
					placeholderTextColor = "#767676"
					selectionColor="#fff"
					keyboardType="email-address"
				/>
			
				<TextInput style={styles.inputBox}
					placeholder="Password"
					secureTextEntry={true}
					placeholderTextColor = "#767676"
				/>
			
				<TouchableOpacity style={styles.button}>
					<Text style={styles.buttonText}>LOGIN</Text>
				</TouchableOpacity>
				<View>
					<TouchableOpacity style={styles.links}>
						<Text style={styles.linkBottom}>Forgot Password</Text>
					</TouchableOpacity>

					<TouchableOpacity style={styles.links}>
						<Text style={styles.linkBottom}>Sign Up</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}	
		
const styles = StyleSheet.create({
	container : {
		flexGrow: 1,
		justifyContent:'center',
		alignItems: 'center',
		backgroundColor:'#3F5169'
	},
	backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
	inputBox: {
		width:350,
		paddingHorizontal:16,
		fontSize:16,
		color:'#f6f6f6',
		marginVertical: 10,
		borderWidth:0,
		borderBottomWidth:1,
		borderColor:'#767676'
	},
	
	button: {
		width:350,
		backgroundColor:'#03B5D1',
		borderRadius: 5,
		marginVertical: 10,
		paddingVertical: 13,
		color:'#FFFFFF'
	},
	
	buttonText: {
		fontSize:20,
		fontWeight:'500',
		color:'#ffffff',
		textAlign:'center'
	},
	logo:{
		marginBottom:20
	},
	linkBottom: {
		marginTop:20,
		color:'#cdcdcd',
		textDecorationLine:'underline'
	},
	links:{
		alignItems:'center'
	}

		
});
		