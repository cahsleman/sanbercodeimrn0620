import {createStackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';


import React from 'react';
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';

const Navigation=createStackNavigator({
Login: {
    screen: LoginScreen,
},
About: {
    screen: AboutScreen,
},
});
export default Navigation;