import React from 'react';
import { View, Text, FlatList, StyleSheet, Image } from 'react-native';
import SkillItem from './components/SkillItem';
import skillData from './skillData.json';

export default class SkillScreen extends React.Component {

  render() {

    return (
      <View style={styles.container}>

        <View style={styles.navBar}>
          <Text style={styles.headerText}>PROFILE</Text>
        </View>

        <View style={styles.profilContainer}>
            <View style={styles.containerPic}>
                <View style={{height:200}}>
                    <Text style={{marginTop:30,marginLeft:30,fontSize:35,color:'#ECECEC'}}>John Doe</Text>
                    <Text style={{marginTop:5,marginLeft:30,fontSize:15, color:'#BEB9B9'}}>doe.john@sanbercode.com</Text>
                </View>
                <View style={{height:200, alignItems:'center',justifyContent:'center',marginLeft:40}}>
                    <Image source={require('../Tugas13/images/nopic.png')} style={styles.avatarImg} />
                </View>
            </View>
        </View>

        <View style={{borderBottomWidth:5,borderBottomColor:'#003366'}}>
          <Text style={{fontSize:30,color:'#003366', padding:10}}>SKILL</Text>
        </View>

        <View style={styles.boxKategori}>
          <View style={styles.textKategoriContainer}>
            <Text style={styles.textKategori}>Library / Framework</Text>
          </View>
          <View style={styles.textKategoriContainer}>
            <Text style={styles.textKategori}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.textKategoriContainer}>
            <Text style={styles.textKategori}>Teknologi</Text>
          </View>
        </View>

        <FlatList 
            data={skillData.items}
            renderItem={(skill)=><SkillItem skill={skill.item}/>}
            keyExtractor={(item)=>item.id}
            ItemSeparatorComponent={()=><View style={{height:10}} />}
          />

      </View>
    );
  }

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar:{
    alignItems:'flex-start',
    justifyContent: 'center',

    backgroundColor:'#3F5169',
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5
  },
  containerPic:{
    height:200,
    flexDirection: 'row',
    //justifyContent:'center'
  }, 
  header: {
    backgroundColor: 'white',
    alignItems: 'flex-end',
    justifyContent: 'center',
    height:150
  },
  headerText:{
    color:'white',
    padding:24,
    fontSize:20
},
avatarImg:{
    width:150,
    height: 150,
    borderWidth:2,
    borderColor:'#CCC',
    borderRadius: 200 / 2,
    overflow: "hidden",
},
  profilContainer: {
    //justifyContent: 'space-between',
    backgroundColor:'#466582'
  },
  profilDetails: {
    paddingHorizontal: 15,
    flex: 1
  },

  boxKategori: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  textKategoriContainer: {
    padding: 5,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#466582',
    marginRight:2,
    width:135
  },
  textKategori: {
    fontWeight: 'bold',
    color:'white',
    textAlign:'center'
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  }
});

