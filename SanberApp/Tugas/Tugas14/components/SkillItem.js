import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillItem extends Component {
	render() {
		let skill = this.props.skill;
		return (
			<View style={styles.container}>
				<View style={{justifyContent:'center'}}>
					<Icon name={skill.iconName} size={80} color="#003366"/>
				</View>
				<View style={styles.skillDetails}>
					<Text style={styles.skillTitle}>{skill.skillName}</Text>
					<Text style={styles.skillKategori}>{skill.categoryName}</Text>
					<Text style={styles.skillStat}>{skill.percentageProgress}</Text>
				</View>
				<TouchableOpacity style={styles.tabItem}>
					<Icon name="chevron-right" size={90} color="#003366"/>
				</TouchableOpacity>
			</View>
		)
	}
};

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		paddingTop: 10,
		paddingLeft: 10,
		paddingBottom: 10,
		backgroundColor: 'white',
		//borderRadius: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 5,
		},
		shadowOpacity: 0.36,
		shadowRadius: 6.68,
		borderTopWidth:1,
		marginLeft:10,
		marginRight:10,
		elevation: 11,
		borderTopColor:'#466582'
	},
	skillTitle: {
		fontSize: 16,
		color: '#003366',
		fontWeight: 'bold'
	},
	skillDetails: {
		flex: 1,
		paddingHorizontal: 15,
	},
	skillKategori: {
		fontSize: 13,
		paddingTop: 3,
		fontWeight: 'bold',
		color: '#3EC6FF'
	},
	skillStat: {
		fontSize: 30,
		fontWeight: 'bold',
		color: '#466582',
		alignSelf: 'flex-end'
	},
	tabItem: {
		// width: 50,
		flexDirection: 'row',
		justifyContent: 'center',
	}
});