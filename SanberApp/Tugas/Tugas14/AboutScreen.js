import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, TextInput, Icon, TouchableOpacity } from 'react-native'
import SkillScreen from './SkillScreen';
import ProjectScreen from '../TugasNavigation/ProjectScreen'
import AddScreen from '../TugasNavigation/AddScreen'

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";



export const AboutScreen = ({ navigation }) => (
	
	<View style={styles.container}>
		<View style={styles.navBar}>
			<Image source={require('./images/list.png')} style={styles.navBarMenuIconList} />
		</View>

		<View style={styles.avatarBox}>
			<Image source={require('./images/nopic.png')} style={styles.avatarImg} />
		</View>	

		<View style={styles.userProfileView}>
			<Text style={styles.userProfileName}>CAHSLEMAN</Text>
			<Text style={styles.userProfileTitle}>DEVELOPER</Text>
			
		</View>
		<View  style={styles.userProfileCaption}>
			<Text style={styles.captionBox}>
			Hi, i’m cahsleman. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non nunc vel felis venenatis pellentesque. Donec mattis iaculis diam vel suscipit. 
			</Text>
		</View>

		<View style={styles.portofolioBox}>
			<Text style={{color:'#ACA4A4', fontSize:16}}>Portofolio : </Text>
		</View>
		<View style={styles.portofolioIcon}>
			<TouchableOpacity style={styles.iconPlacement}><Image source={require('./images/facebook.png')} style={styles.icon} /></TouchableOpacity>
			<TouchableOpacity style={styles.iconPlacement}><Image source={require('./images/instagram.png')} style={styles.icon} /></TouchableOpacity>
			<TouchableOpacity style={styles.iconPlacement}><Image source={require('./images/twitter.png')} style={styles.icon} /></TouchableOpacity>
		</View>
		<View style={styles.portofolioBox}>
			<Text style={{color:'#ACA4A4', fontSize:16}}>My Project : </Text>
		</View>
		<View style={styles.portofolioIcon}>
			<TouchableOpacity style={styles.iconPlacement}><Image source={require('./images/github.png')} style={styles.icon} /></TouchableOpacity>
			<TouchableOpacity style={styles.iconPlacement}><Image source={require('./images/bitbucket.png')} style={styles.icon} /></TouchableOpacity>
		</View>
	</View>



);


const styles = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'#466582'
	},
	navBar:{
		marginTop:0,
		backgroundColor:'#3F5169',
		height:90,
		shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity:  0.4,
        shadowRadius: 3,
        elevation: 5
	},
	navBarMenu:{
		flexDirection:'row',
		justifyContent:'center',
		alignItems: 'center',
	},
	navBarMenuIconList:{
		width:30,
		height:25,
		marginTop:40,
		marginLeft:20
	},
	navBarText: {
		
	},
	avatarBox:{
		marginTop:50,
		alignItems: 'center',
		justifyContent:'center',
		//height:150,
		//width:150
	},
	avatarImg:{
		width:200,
		height: 200,
		borderWidth:2,
		borderColor:'#CCC',
		borderRadius: 200 / 2,
    	overflow: "hidden",
	},
	userProfileView:{
		marginTop:30,
		alignItems: 'center',
		justifyContent:'center',
	},
	userProfileName:{
		fontSize:30,
		color:'#FFF9F9'
	},
	userProfileTitle:{
		fontSize:14,
		color: '#ACA4A4'
	},
	userProfileCaption:{
		flexDirection:'row',
		marginTop: 30,
		marginLeft: 50,
		marginRight: 50,
		textAlign:'center',
		borderBottomWidth:1,
		borderColor:'#ACA4A4',
		paddingBottom: 30
	},
	captionBox:  {
		color:'#ACA4A4',
		textAlign:'center'
	},
	portofolioBox: {
		marginTop: 20,
		marginLeft: 50,
		marginRight: 50,
		
	},
	portofolioIcon: {
		flexDirection:'row',
		justifyContent:'center',
	},
	iconPlacement: {
		width:120,
		alignItems:'center',
		marginTop:20

	},
	icon:{
		width: 50,
		height: 50
	}
	
});