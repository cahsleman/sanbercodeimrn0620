var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function bacaBuku(time,buku,i=0){
	if (buku.length > i) {
		readBooks(time,buku[i], function(sisa){
			if (sisa > 0) {
	        	bacaBuku(sisa,buku,i+1);
			}
		});
	}
}
bacaBuku(10000,books);