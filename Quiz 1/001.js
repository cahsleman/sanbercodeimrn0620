function balikString(stringInput){
	var arrString = [];
	for( var i =0; i<stringInput.length;i++){
		arrString[i] = stringInput[i];
	}

	var balik = "";
	for( var j=arrString.length-1; j>=0; j--){
		balik += arrString[j];
	}
	return balik;
}

function palindrome(inputKata){
  var balikKata = balikString(inputKata);
  if( inputKata.toLowerCase() == balikKata.toLowerCase()){
    return true;
  }else{
    return false;
  }
}

function bandingkan(num1 = 0, num2 = 0){

  if( num1 < 0 || num2 < 0 ){
    return -1;
  }

  if( num1 > 0 || num2 > 0 )
  {
    if( num1 > num2 ){
      return num1;
    }else if( num2 > num1 ){
      return num2;
    }else if( num1 == num2 ){
      return -1;
    }
  }else{
    return -1;
  }
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
console.log('------------------------------------');
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
console.log('------------------------------------');
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18