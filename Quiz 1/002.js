function AscendingTen(inputNumber){
  if( typeof inputNumber == 'undefined' ){
    return -1;
  }

  var output = "";
  for(var i=inputNumber;i<(10+inputNumber);i++){
    output += i+" "
  }
  return output;
}


function DescendingTen (inputNumber){
  if( typeof inputNumber == 'undefined' ){
    return -1;
  }

  var output = "";
  for(var i=inputNumber;i>(inputNumber-10);i--){
    output += i+" ";
  }
  return output;
}

function ConditionalAscDesc(ref, check) {
  // Tulis code kamu di sini
  var str = ""
  if (ref != null && check != null) {
    if (check % 2 == 1) {
      return AscendingTen(ref);
    } else {
      return DescendingTen(ref);
    }
    return str
  } else {
    return "-1"
  }
}

function ularTangga() {
  // Tulis code kamu di sini
  var startNum = "";
  var firstNum = 100;
  var tangga = 1;
  var increment = 1;

  while (firstNum > 0) {
    if (increment > 10){
      increment = 1
      startNum += "\n"
      tangga++
      firstNum = firstNum - 10
    }
    if (increment != 1){
      if (tangga % 2 == 0) {
        firstNum++
      } else {
        firstNum--
      }
    }
    if (firstNum > 0) { startNum += firstNum + " "}
    increment++
  }
  return startNum
}



console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

console.log(ularTangga()) 
/* 
Output : 
  100 99 98 97 96 95 94 93 92 91
  81 82 83 84 85 86 87 88 89 90
  80 79 78 77 76 75 74 73 72 71
  61 62 63 64 65 66 67 68 69 70
  60 59 58 57 56 55 54 53 52 51
  41 42 43 44 45 46 47 48 49 50
  40 39 38 37 36 35 34 33 32 31
  21 22 23 24 25 26 27 28 29 30
  20 19 18 17 16 15 14 13 12 11
  1 2 3 4 5 6 7 8 9 10
*/