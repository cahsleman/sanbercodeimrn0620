console.log('TUGAS 1 - LOOPING WHILE');
console.log('LOOPING PERTAMA ');

var i = 1;
while( i <= 20){
	if(i%2 == 0){
 		console.log(i+' - I love coding');
	}
	i++;
}

console.log('\n\n\nLOOPING KEDUA');
var j = 20;
while( j > 1){
	if(j%2 == 0){
		console.log(j+' - I will become a mobile developer');
	}
	j--;
}

console.log('\n\n\nTUGAS 2 - LOOPINg FOR');
for(var k=1; k <= 20; k++ ){
	if(k%2 == 0){
		console.log(k+' - Berkualitas');
	}else if(k%3 == 0 && k%2==1 ){
		console.log(k+' - I Love Coding');
	}else{
		console.log(k+' - Santai');
	}
}

console.log('\n\n\nTUGAS 3 PERSEGI PANJANG');
for ( i = 1; i <= 4; i++) {
  var str = '';
    for ( j = 1; j <= 8; j++) {
      str += '#';
    }
    console.log(str);
}

console.log('\n\n\nTUGAS 4 - PIRAMID');

var str = '#';
for (i = 1; i <= 7; i++) {
  console.log(str.repeat(i));
}

console.log('\n\n\nTUGAS 5 - PAPAN CATUR');

for ( i = 1; i <= 8; i++) {
  var str = '';
    for ( j = 1; j <= 8; j++) {
      if(i%2 === 0){
        str += (j%2 === 0) ? ' ' : '#';
      }else{
        str += (j%2 === 0) ? '#' : ' ';
      }
    }
    console.log(str);
}