class Score {
  constructor(subject,points,email){
    this.subject = subject;
    this.points = points;
    this.email = email;
  }

  average(){
    if( Array.isArray(this.points) ){
      var pointData = this.points;
      var avg = 0;
      for(var i=0; i<pointData.length;i++){
        avg += this.points[i];
      }
      var rataRata = (avg/pointData.length);
      return rataRata;
    }else{
      return this.points;
    }
  }
}

var coba = new Score('',1,'');

function viewScores(data, subject) {
  // code kamu di sini
  let [txt,...student] = data
  var arrayNilai = []

  for (var i = 0; i < student.length; i++) {
    arrayNilai[i] = {email:student[i][0],subject:subject,points:student[i][key(txt,subject)]}
  }

  console.log(arrayNilai)
}

var key = (arr,val) => {
  for (var i = 0; i < arr.length; i++) {
    var trimmed_value = arr[i].replace(/\s/g,"")
    if (trimmed_value == val) {
      return i
    }
  }
}

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
];

viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");


function recapScores(data) {

  let [txt,...student] = data

  for (var i = 0; i < student.length; i++) {
    let [email,...points] = student[i]
    var nilai = new Score('',points,email).average()

    console.log(`${i+1}. Email: ${email}`)
    console.log(`Rata-rata: ${nilai}`)

    if (nilai > 70) {
      console.log('Predikat: participant')
    } else if(nilai > 80) {
      console.log('Predikat: graduate')
    } else if(nilai > 90) {
      console.log('Predikat: honour')
    }
  }
}

recapScores(data);